# Ansible S3 Sync Service

Ansible S3 Sync Service can be used to backup your directories (say logs) to a desired S3 bucket.

  - Backup logs to S3
  - Configurable
  - Backups your directories on graceful shutdown

### Version
0.0.10

### Prerequisites

This service is designed and tested to be used on:
  - Ubuntu 14.04 LTS
  - Ansible 2.1.0.0

### Installation

You need ansible installed via apt-get or pip:

```sh
# apt-get install ansible
```

or

``` sh
# pip install ansible
```

### Configure

You have setup defaults available in the following YAML file.

**/roles/s3/vars/main.yml**

``` 
s3cmd_user: "gursimran"
aws_access_key: "AKIAJEGxxxxxx"
aws_secret_key: "QBvJmeY665mvLAKu0aQ/kBxxxxxx"
interval: 5
dirs:
  - {bucket_name: "test1", directory: "/tmp/log1"}
  - {bucket_name: "test2", directory: "/tmp/log2"}
```

### Run

In order to override these attributes during run time, you can pass extra variables during run time as follwoing example.

```sh
ansible-playbook -e "@dirs.json" -e "interval=6" logs-to-s3.yml 
```

 In order to change directories during runtime, change dir.json with desired **bucket_name** and **directory**.



License
----

MIT


**Free Software, Hell Yeah!**